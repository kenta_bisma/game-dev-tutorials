extends KinematicBody2D

var speed = 100
export (int) var GRAVITY = 500
export (int) var jump_speed = -200

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var _animated_sprite = $AnimatedSprite

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		_animated_sprite.flip_h = false
		velocity.x = speed
	if Input.is_action_pressed('left'):
		_animated_sprite.flip_h = true
		velocity.x = -speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		_animated_sprite.play("jump")
	elif velocity.x != 0:
		_animated_sprite.play("run")
		if velocity.x > 0:
			_animated_sprite.flip_h = false
		else:
			_animated_sprite.flip_h = true
	else:
		_animated_sprite.play("idle")
	
