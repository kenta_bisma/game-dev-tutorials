extends LinkButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


export(String) var scene_to_load

func _on_LinkButton_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
