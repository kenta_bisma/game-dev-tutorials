extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var jumped = false

func get_input():
	velocity.x = 0
	if is_on_floor():
		jumped = false
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
	if !is_on_floor() and Input.is_action_just_pressed('up') and !jumped:
		velocity.y = jump_speed
		jumped = true
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_mouse_button_pressed(1):
		var x = 1 if velocity.x > 0 else 0 if velocity.x == 0 else -1
		var y = 1 if velocity.y > 0 else 0 if velocity.y == 0 else -1
		velocity.x += speed * x
		velocity.y += jump_speed * y
		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
	
